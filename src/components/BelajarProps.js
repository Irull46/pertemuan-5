import React from 'react';
import { Image, Text, View } from 'react-native';

const BelajarProps = props => {
    return (
        <View style={{alignItems: 'center', marginRight: 15}}>
            <Image source={props.foto} style={{width: 70, height: 70, borderRadius: 70/2}} />
            <Text style={{fontSize: 14, color: '#FFFFFF', maxWidth: 60, textAlign: 'center'}}>{props.teks}</Text>
        </View>
    );
};

export default BelajarProps;
