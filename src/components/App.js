import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import { BelajarProps, BelajarState } from '../components';
import { Klepon, NasiGoreng, TempeGoreng } from '../assets';

const App = () => {
  return (
    <View>
      <Text style={{color: '#FFFFFF', marginBottom: 40}}>Belajar Props</Text>

      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View style={{flexDirection: 'row'}}>
          <BelajarProps foto={Klepon} teks='Klepon Ajaib' />
          <BelajarProps foto={NasiGoreng} teks='Klepon Cinta' />
          <BelajarProps foto={TempeGoreng} teks='Klepon Madura' />
          <BelajarProps foto={Klepon} teks='Klepon Cina' />
          <BelajarProps foto={NasiGoreng} teks='Klepon Cincau' />
          <BelajarProps foto={TempeGoreng} teks='Klepon Manis' />
        </View>
      </ScrollView>

      <Text style={{color: '#FFFFFF', marginTop: 40}}>Belajar State</Text>

      <View style={{marginTop: 40}}>
        <BelajarState />
      </View>
    </View>
  );
};

export default App;
