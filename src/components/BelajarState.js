import React, { useState } from 'react';
import { View, Text, Button } from 'react-native';

const BelajarState = () => {
    const [angka, setAngka] = useState(0);
    return (
        <View>
            <Text style={{color: '#FFFFFF'}}>{angka}</Text>
            <Button title="Tambah" onPress={() => setAngka(angka + 1)} />
        </View>
    );
};

export default BelajarState;
